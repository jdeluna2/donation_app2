json.array!(@donations) do |donation|
  json.extract! donation, :id, :donor_id, :amount, :date
  json.url donation_url(donation, format: :json)
end
